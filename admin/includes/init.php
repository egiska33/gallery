<?php
define('DS') ? null : define('DS',DIRECTORY_SEPARATOR);
define('DS') ? null : define('SITE_ROOT', DS. 'var'.DS.'www'.DS.'public'.DS.'gallery');
define('DS') ? null : define('INCLUDES_PATH', SITE_ROOT.DS.'admin'.DS.'includes');

require_once 'functions.php';
require_once 'new_config.php';
require_once 'database.php';
require_once 'db_objects.php';
require_once 'user.php';
require_once 'db_objects.php';
require_once 'session.php';