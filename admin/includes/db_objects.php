<?php

/**
 * Class Db_objects
 */
class Db_objects {

    //    method for find all users

    /**
     * @return array
     */
    public static function find_all()
    {
        return static::find_this_query("SELECT * FROM " .static::$db_table. " ");
    }


//    method for find one by id

    /**
     * @param $user_id
     * @return bool|mixed
     */
    public static function find_by_id($id)
    {
        global $database;

        $result_array = static::find_this_query("SELECT * FROM ".static::$db_table." WHERE id= $id LIMIT 1");

//        kitas uzrasymas if salygos
        return !empty($result_array) ? array_shift($result_array) : false;
    }
//    metodas queriui, kuris is duombazes paima duomenys ir priskiria juos masyvui

    /**
     * @param $sql
     * @return array
     */
    public static function find_this_query($sql)
    {
        global $database;

        $result = $database->query($sql);
        $the_object_array = array();

        while ($row = mysqli_fetch_array($result))
        {
            $the_object_array[] = static::instantation($row);
        }

        return $the_object_array;
    }
//    statinis metodas objekto properciams priskirti reiksmes

    /**
     * @param $the_record
     * @return mixed
     */
    public static function instantation($the_record)
    {
        $calling_class = get_called_class();
        $the_object = new $calling_class;
//        $the_object->id        = $found_user['id'];
//        $the_object->username  = $found_user['username'];
//        $the_object->password  = $found_user['password'];
//        $the_object->firstName = $found_user['firstName'];
//        $the_object->lastName  = $found_user['lastName'];

        foreach ($the_record as $the_atribute => $value)
        {
            if ($the_object->has_the_atribute($the_atribute))
            {
                $the_object->$the_atribute = $value;
            }
        }

        return $the_object;

    }
    //    privatus metodas objekto properciams

    /**
     * @param $property
     * @return bool
     */
    private function has_the_atribute($property)
    {
        $object_properties = get_object_vars($this);

        return array_key_exists($property, $object_properties);
    }

    /**
     * @return array
     */
    protected function properties()
    {
//        return get_object_vars($this);
        $properties = array();
        foreach (static::$db_table_fields as $dbTableField){
            if(property_exists($this, $dbTableField)){
                $properties[$dbTableField] = $this->$dbTableField;
            }
        }
        return $properties;
    }


    /**
     * @return array
     */
    protected function clean_properties()
    {
        global $database;
        $clean_properties = array();

        foreach ($this->properties() as $key => $value){
            $clean_properties[$key] = $value;
        }
        return $clean_properties;
    }


    /**
     * @return bool
     */
    public function save()
    {
        return (isset($this->id)) ? $this->update() : $this->create();
    }


    /**
     * @return bool
     */
    public function create()
    {
        global $database;

        $properties = $this->clean_properties();
        $sql = "INSERT INTO " .static::$db_table. " ( " . implode(",", array_keys($properties)). " ) ";
        $sql .= "VALUES (' ". implode(" ',' ", array_values($properties)) . "')";

        if($database->query($sql)){

            $this->id = $database->the_insert_id();
            return true;

        } else {
            return false;
        }
    }


    /**
     * [update description]
     *
     * @return [type] [description]
     */
    public function update()
    {
        global $database;
        $properties = $this->clean_properties();
        $properties_pairs = array();

        foreach ($properties as $key => $value){
            $properties_pairs[] = " {$key} = '{$value}' ";
        }

        $sql = "UPDATE " .static::$db_table. " SET ";
        $sql .= implode(" ,", $properties_pairs);
        $sql .= " WHERE id =  ".$this->id;

        $database->query($sql);

        return (mysqli_affected_rows($database->conection) == 1) ? true : false ;
    }


    /**
     * @return bool
     */
    public function delete()
    {
        global $database;
        $sql = "DELETE FROM ".static::$db_table." WHERE id = '$this->id'";
        $database->query($sql);

        return (mysqli_affected_rows($database->conection) == 1) ? true : false ;
    }


}