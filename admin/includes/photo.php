<?php


class Photo extends Db_objects {

    protected static $db_table = "photos";
    protected static $db_table_fields = array('id','title','description', 'fileName', 'fileType', 'size');
    public $id;
    public $title;
    public $description;
    public $fileName;
    public $fileType;
    public $size;

    public $tmp_path;
    public $uploaded_directory = "images";
    public $errors = array();
    public $upload_errors = array(
        UPLOAD_ERR_OK         => "There no errors",
        UPLOAD_ERR_INI_SIZE   => "The uploaded file exceeds the upload_max_filesize directive",
        UPLOAD_ERR_FORM_SIZE  => "The uploaded file exceeds the MAX_FILE_SIZEdirective",
        UPLOAD_ERR_PARTIAL    => "The uploaded file was only partyally uploaded",
        UPLOAD_ERR_NO_FILE    => "No file was uploaded",
        UPLOAD_ERR_NO_TMP_DIR => "Missing a temporary folder",
        UPLOAD_ERR_CANT_WRITE => "Failed to write file to disc",
        UPLOAD_ERR_EXTENSION  => "A PHP exstension stopped the file upload"
    );

//    This is passing $_FILES['uploaded_file'] as an argument
    public function set_file($file)
    {
        if(empty($file) || !$file || !is_array($file)){
            $this->errors[] = 'There was no file upload here';
            return false;
        } elseif ($file['error'] != 0){
            $this->errors[] = $this->upload_errors_array[$file['error']];
            return false;
        } else {
            $this->fileName = basename($file['name']);
            $this->tmp_path = $file['tmp_name'];
            $this->fileType = $file['type'];
            $this->size = $file['size'];
        }
    }

    public function picture_path()
    {
        return $this->uploaded_directory.DS.$this->fileName;
    }

    public function save()
    {
        if($this->id){
            $this->update();
        } else {
            if(!empty($this->errors)){
                return false;
            }
            if(empty($this->fileName) || empty($this->tmp_path)){
                $this->errors[] = 'The file was not avalaible';
                return false;
            }
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->uploaded_directory.DS.$this->fileName;

            if(file_exists($target_path)){
                $this->errors[] = "The file {$this->fileName} already exist";
                return false;
            }
            if(move_uploaded_file($this->tmp_path, $target_path)){
                if($this->create()){
                    unset($this->tmp_path);
                    return true;
                } else {
                    $this->errors[] = "The file directory probably does not have premision";
                    return false;
                }
            }
        }
    }

}