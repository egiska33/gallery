<?php

require_once ('new_config.php');

class Database {

    public $conection;

    function __construct()
    {
        $this->open_db_conection();
    }


    public function open_db_conection(){

        $this->conection= new mysqli('localhost','root', 'root', 'gallery_db');

    }


    public function query ($sql){

        $result = $this->conection->query($sql);

        $this->confirm_query($result);

        return $result;

    }

    private function confirm_query($result){

        if(!$result){
            die('QUERY FAILED'.$this->conection->error);
        }

    }

    public function escape_string($string){

        $escape_string = mysqli_real_escape_string($this->conection, $string);
        return $escape_string;

    }

    public function the_insert_id(){

        return mysqli_insert_id($this->conection);

    }



}


$database = new Database();