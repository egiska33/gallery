<?php
/**
 * Created by PhpStorm.
 * User: egiska33
 * Date: 20/11/2017
 * Time: 18:46
 */

class User extends Db_objects {

//    userrio lenteles stulpeliai (propertis)
    protected static $db_table = "users";
    protected static $db_table_fields = array('username','password', 'firstName', 'lastName');
    public $id;
    public $password;
    public $username;
    public $firstName;
    public $lastName;

//    patikriname ar yra useris
    public static function verify_user($username, $password)
    {

        global $database;

        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $sql = "SELECT * FROM ".self::$db_table." WHERE username = '{$username}' AND password = '{$password}' LIMIT 1";

        $result_array = self::find_this_query($sql);

        return !empty($result_array) ? array_shift($result_array) : false;
    }

}